package controllers.application;

import configurations.serialization.JsonHelper;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import models.base.BaseModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import services.BaseService;
import services.SecurityService;
import services.util.api.request.ApiParams;
import services.util.api.response.Page;
import services.util.api.response.ResultMapper;


/**
 * Created By:  Riyaz
 * Date:        26 July, 2019
 */

@Slf4j
@Getter
public abstract class BaseController<Model extends BaseModel> {

    protected abstract BaseService getService();

    private static final Logger logger = LogManager.getLogger(BaseController.class.getName());

    @Autowired
    private JsonHelper jsonHelper;

    @Autowired
    protected SecurityService securityService;

    protected BaseService<Model> service;

    @Autowired
    protected ResultMapper resultMapper;

    @GetMapping(value="/{id}", produces = "application/json")
    public ResponseEntity getCurrency(@PathVariable Long id, @RequestParam(value = "include", required = false) String include) {
        try {
            BaseModel model = (include==null) ? getService().find(id) : getService().find(id, ApiParams.getInstance().buildInclude(include));
            return resultMapper.map(model);
        }
        catch (RuntimeException ex){
            return resultMapper.map(ex);
        }
    }

    @GetMapping
    public ResponseEntity getAll(@RequestParam(value = "filters", required = false) String filters,
                                 @RequestParam(value = "include", required = false) String include,
                                 @RequestParam(value = "sort", required = false) String sort,
                                 @RequestParam(value = "start", required = false) Long start,
                                 @RequestParam(value = "count", required = false) Long count ) {
        try {
            ApiParams params= new ApiParams(filters, include, sort, start, count);
            Page page = getService().getAll(params);
            return resultMapper.map(page);
        }catch (RuntimeException rex){
            logger.error("Error getting " + getService().getTypeClass() + " entities with filters: "+ filters, rex);
            return resultMapper.map(rex);
        }
    }
}