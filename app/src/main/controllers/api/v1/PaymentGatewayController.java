package controllers.api.v1;

import controllers.application.BaseController;
import models.entities.PaymentGatewayModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import services.BaseService;
import services.PaymentGatewayService;

/**
 * Created By:  Riyaz
 * Date:        05 September, 2019
 */

@RestController
@RequestMapping("/api/v1/payment-gateway")
public class PaymentGatewayController extends BaseController<PaymentGatewayModel> {

    @Autowired
    private PaymentGatewayService paymentGatewayService;

    @Override
    protected BaseService getService() {
        return paymentGatewayService;
    }




}
