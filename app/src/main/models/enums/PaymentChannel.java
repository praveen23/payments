package models.enums;

public enum PaymentChannel {
    CREDIT_CARD,
    DEBIT_CARD
}
