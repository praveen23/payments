package models.enums;

public enum PaymentStatus {
    DECLINED,
    ACCEPTED
}
