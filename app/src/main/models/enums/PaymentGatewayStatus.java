package models.enums;

public enum PaymentGatewayStatus {
    ACTIVE,
    INACTIVE
}
