package models.dao;

import lombok.Data;
import lombok.EqualsAndHashCode;
import models.base.BaseDao;
import models.entities.PaymentsModel;
import org.springframework.stereotype.Repository;

@EqualsAndHashCode(callSuper=false)
@Data
@Repository
public class PaymentsDao extends BaseDao<PaymentsModel> {
}
