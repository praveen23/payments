package models.dao;

import lombok.Data;
import lombok.EqualsAndHashCode;
import models.base.BaseDao;
import models.entities.PaymentRequestModel;
import org.springframework.stereotype.Repository;

@EqualsAndHashCode(callSuper=false)
@Data
@Repository
public class PaymentRequestDao extends BaseDao<PaymentRequestModel> {
}
