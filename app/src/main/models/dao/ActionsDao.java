package models.dao;

import lombok.Data;
import lombok.EqualsAndHashCode;
import models.base.BaseDao;
import models.entities.ActionsModel;
import org.springframework.stereotype.Repository;

@EqualsAndHashCode(callSuper=false)
@Data
@Repository
public class ActionsDao extends BaseDao<ActionsModel> {
}
