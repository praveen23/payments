package models.dao;

import lombok.Data;
import lombok.EqualsAndHashCode;
import models.base.BaseDao;
import models.entities.PaymentGatewayModel;
import org.springframework.stereotype.Repository;


@EqualsAndHashCode(callSuper=false)
@Data
@Repository
public class PaymentGatewayDao extends BaseDao<PaymentGatewayModel> {


}
