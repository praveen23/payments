package models.dao;

import lombok.Data;
import lombok.EqualsAndHashCode;
import models.base.BaseDao;
import models.entities.EventsModel;
import org.springframework.stereotype.Repository;

@EqualsAndHashCode(callSuper=false)
@Data
@Repository
public class EventsDao extends BaseDao<EventsModel> {
}
