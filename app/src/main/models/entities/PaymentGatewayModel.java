package models.entities;

import lombok.Getter;
import lombok.Setter;
import models.enums.PaymentGatewayStatus;
import models.base.BaseModel;

import javax.persistence.*;

@Entity
//@SQLDelete(sql = "UPDATE payment_gateway SET status = 'INACTIVE' WHERE id = ?")
@Table(name = "payment_gateway")
@Getter
@Setter
public class PaymentGatewayModel extends BaseModel {

    @Basic
    @Column(name = "name", nullable = false)
    private String name;

    @Basic
    @Column(name = "short_name", nullable = true)
    private String shortName;

    @Basic
    @Column(name = "code", nullable = false)
    private String Code;

    @Basic
    @Column(name = "description", nullable = true)
    private String description;

    @Basic
    @Column(name = "status", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PaymentGatewayStatus status;

}
