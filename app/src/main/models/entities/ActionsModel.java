package models.entities;
import lombok.Getter;
import lombok.Setter;
import models.base.BaseModel;

import javax.persistence.*;

@Entity
//@SQLDelete(sql = "UPDATE payment_gateway SET status = 'INACTIVE' WHERE id = ?")
@Table(name = "actions")
@Getter
@Setter

public class ActionsModel extends BaseModel{

    @Basic
    @Column(name = "name" , nullable = false)
    private String name;
}
