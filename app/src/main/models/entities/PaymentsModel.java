package models.entities;
import lombok.Getter;
import lombok.Setter;
import models.base.BaseModel;
import models.enums.PaymentChannel;
import models.enums.PaymentStatus;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
//@SQLDelete(sql = "UPDATE payment_gateway SET status = 'INACTIVE' WHERE id = ?")
@Table(name = "payments")
@Getter
@Setter

public class PaymentsModel extends BaseModel {

    @ManyToOne
    @JoinColumn(name = "request_id", referencedColumnName = "id", nullable = false)
    private PaymentRequestModel paymentRequestId;

    @Basic
    @Column(name = "amount_captured" , nullable = false)
    private Double amountCaptured;

    @Basic
    @Column(name = "status" , nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PaymentStatus status;

    @ManyToOne
    @JoinColumn(name = "gateway_id", referencedColumnName = "id", nullable = false)
    private PaymentGatewayModel paymentGatewayId;

    @Basic
    @Column(name = "gateway_charge" , nullable = false)
    private Double gatewayCharge;

    @Basic
    @Column(name = "gateway_message" , nullable = true)
    private String gatewayMessage;

    @Basic
    @Column(name = "gateway_txn_id" , nullable = false)
    private String gatewayTxnId;

    @Basic
    @Column(name = "payment_channel" , nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PaymentChannel paymentChannel;

    @Basic
    @Column(name = "gateway_error" , nullable = false)
    private Boolean gatewayError;

    @Basic
    @Column(name = "gateway_time_taken" , nullable = false)
    private BigInteger gatewayTimeTaken;

    @Basic
    @Column(name = "gateway_receipt_url" , nullable = true)
    private String gatewayReceiptUrl;

    @Basic
    @Column(name = "gateway_failure_id" , nullable = true)
    private BigInteger gateway_failure_id;

    @Basic
    @Column(name = "is_disputed" , nullable = false)
    private Boolean isDisputed;

    @Basic
    @Column(name = "disputed_payment_id" , nullable = true)
    private BigInteger disputedPaymentId;


}
