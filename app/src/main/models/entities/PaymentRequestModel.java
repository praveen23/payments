package models.entities;

import lombok.Getter;
import lombok.Setter;
import models.base.BaseModel;
import models.enums.PaymentRequestStatus;
import models.enums.PaymentRequester;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
//@SQLDelete(sql = "UPDATE payment_gateway SET status = 'INACTIVE' WHERE id = ?")
@Table(name = "payment_request")
@Getter
@Setter

public class PaymentRequestModel extends BaseModel {

    @Basic
    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Basic
    @Column(name = "amount" , nullable = false)
    private Double amount;

    @Basic
    @Column(name = "requester" , nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PaymentRequester requester;

    @Basic
    @Column(name = "requester_entity_id" , nullable = true)
    private BigInteger requesterEntityId;

    @Basic
    @Column(name = "requester_entity_name", nullable = true)
    private String requesterEntityName;

    @Basic
    @Column(name = "callback_url", nullable = true)
    private String callbackUrl;

    @Basic
    @Column(name = "preferred_gateway_id", nullable = true)
    private String preferredGatewayId;

    @Basic
    @Column(name = "status" , nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PaymentRequestStatus status;
}
