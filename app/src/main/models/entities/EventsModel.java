package models.entities;
import lombok.Getter;
import lombok.Setter;
import models.base.BaseModel;


import javax.persistence.*;

@Entity
//@SQLDelete(sql = "UPDATE payment_gateway SET status = 'INACTIVE' WHERE id = ?")
@Table(name = "events")
@Getter
@Setter

public class EventsModel extends BaseModel {


    @ManyToOne
    @JoinColumn(name = "gateway_id", referencedColumnName = "id", nullable = true)
    private PaymentGatewayModel paymentGatewayId;

    @ManyToOne
    @JoinColumn(name = "payment_id", referencedColumnName = "id", nullable = true)
    private PaymentsModel paymentId;

    @ManyToOne
    @JoinColumn(name = "payment_request_id", referencedColumnName = "id", nullable = true)
    private PaymentRequestModel paymentRequestId;

    @ManyToOne
    @JoinColumn(name = "gateway_failure_id", referencedColumnName = "id", nullable = true)
    private GatewayFailuresModel gatewayFailuresId;

    @Basic
    @Column(name = "action_taken", nullable = false)
    private Long actionTaken;

    @Basic
    @Column(name = "message", nullable = true)
    private String message;

    @Basic
    @Column(name = "notes", nullable = true)
    private String notes;

}
