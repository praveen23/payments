package models.entities;
import lombok.Getter;
import lombok.Setter;
import models.base.BaseModel;

import javax.persistence.*;

@Entity
//@SQLDelete(sql = "UPDATE payment_gateway SET status = 'INACTIVE' WHERE id = ?")
@Table(name = "gateway_failures")
@Getter
@Setter
public class GatewayFailuresModel extends BaseModel {

    @ManyToOne
    @JoinColumn(name = "gateway_id", referencedColumnName = "id", nullable = false)
    private PaymentGatewayModel paymentGatewayId;

    @ManyToOne
    @JoinColumn(name = "payment_id", referencedColumnName = "id", nullable = false)
    private PaymentsModel paymentId;

    @ManyToOne
    @JoinColumn(name = "payment_request_id", referencedColumnName = "id", nullable = false)
    private PaymentRequestModel paymentRequestId;

    @Basic
    @Column(name = "gateway_status", nullable = false)
    private String gatewayStatus;

    @Basic
    @Column(name = "gateway_code", nullable = true)
    private String gatewayCode;

    @Basic
    @Column(name = "gateway_message", nullable = false)
    private String gatewayMessage;

    @Basic
    @Column(name = "gateway_response", nullable = true)
    private String gatewayResponse;

}
