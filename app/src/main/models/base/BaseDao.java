package models.base;

import exceptions.ResourceNotFoundException;
import exceptions.ServiceUnavailableException;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import services.SecurityService;
import services.util.api.request.ApiParams;
import services.util.api.response.Page;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created By:  Riyaz
 * Date:        16 July, 2019.
 */

@Data
public abstract class BaseDao<Model extends BaseModel> {

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    private SecurityService securityService;

    public static final Integer LOCK_WAIT  = 500;

    public <Model> Model find(Class<Model> modelClass, Long id) {
        Model model = entityManager.find(modelClass, id);
        if(model == null){
            throw new ResourceNotFoundException(modelClass.getName()+" entity not found for id: "+id);
        }
        return model;
    }

    @Transactional(rollbackFor = Exception.class)
    public Model save(Model model) {
        if(securityService.loggedInUserExists()){
            model.setLastUpdatedBy(Long.valueOf(securityService.getCurrentUserBouncerId()));
        }
        entityManager.persist(model);
        return model ;
    }

    @Transactional(rollbackFor = Exception.class)
    public Model edit(Model model) {
        if(securityService.loggedInUserExists()){
            model.setLastUpdatedBy(Long.valueOf(securityService.getCurrentUserBouncerId()));
        }
        entityManager.merge(model);
        return model;
    }

    public void flush(){
        entityManager.flush();
    }

    public Model refresh(Model model){
        entityManager.refresh(model);
        return model;
    }

    @Transactional(rollbackFor = Exception.class)
    public Model remove(Class<Model> modelClass, Long id) {
        Model model = find(modelClass, id);
        if(securityService.loggedInUserExists()){
            edit(model);
        }
        entityManager.remove(model);
        return model;
    }

    @Transactional(rollbackFor = Exception.class)
    public Model remove(Model model) {
        if(securityService.loggedInUserExists()){
            edit(model);
        }
        entityManager.remove(model);
        return model;
    }

    public Page<Model> getAll(Class<Model> entityClass, ApiParams apiParams){
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        Root<Model> countRoot = countQuery.from(entityClass);
        countQuery.select(criteriaBuilder.count(countRoot));
        countQuery.where(apiParams.getFilter().getPredicates(countRoot, criteriaBuilder, countQuery, true));
        Long count = entityManager.createQuery(countQuery).getSingleResult();

        CriteriaQuery<Model> criteriaQuery = criteriaBuilder.createQuery(entityClass);
        Root<Model> root = criteriaQuery.from(entityClass);
        criteriaQuery.select(root);
        criteriaQuery.where(apiParams.getFilter().getPredicates(root, criteriaBuilder,criteriaQuery, true));
        // Order By
        apiParams.getSort().applyOrderBy(criteriaBuilder, root, criteriaQuery);

        TypedQuery<Model> typedQuery = entityManager.createQuery(criteriaQuery);
        typedQuery.setFirstResult(apiParams.getStart().intValue());
        typedQuery.setMaxResults(apiParams.getCount().intValue());
        List<Model> result = new ArrayList<>();
        try {
            result = typedQuery.getResultList();
        }catch(NoResultException nex){
            // do Nothing.
        }
        return new Page(apiParams.getStart(), count, result);
    }

    @Transactional(rollbackFor = Exception.class)
    public <Model> Model acquireLock(Class<Model> modelClass, Long id, LockModeType lock) {
        try {
            Map<String, Object> properties = new HashMap<>();
            properties.put("javax.persistence.lock.timeout", LOCK_WAIT);
            Model instance = entityManager.find(modelClass, id, lock, properties);
            if (instance == null) {
                throw new ResourceNotFoundException(modelClass.getName() + " entity not found for id: " + id);
            }
            else {
                return instance;
            }
        }
        catch (NoResultException err) {
            throw new ResourceNotFoundException("No "+modelClass.getName()+" entity found for id: "+ id , err);
        }
        catch (RuntimeException err){
            throw new ServiceUnavailableException("Error acquiring lock for "+modelClass.getName()+" entity with id : "+id, err);
        }
    }
}
