package models.base;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;

/**
 * Created By:  Riyaz
 * Date:        16 July, 2019.
 */

@Data
@MappedSuperclass
@Getter
@Setter
public abstract class BaseModel implements Serializable, Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false)
    protected Long id;



    @Basic
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "created_at", nullable = false, updatable = false)
    protected DateTime createdAt;

    @Basic
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "last_updated_at", nullable = false)
    protected DateTime lastUpdatedAt;

    @Basic
    @Column(name = "created_by", updatable = false)
    protected Long createdBy;

    @Basic
    @Column(name = "last_updated_by")
    protected Long lastUpdatedBy;

    @Basic
    @Version
    @Column(name = "version", nullable = false)
    protected Long version = 0L;

    @Transient
    protected HashSet<String> allowedEdits;

    /**
     *  Listeners & Callbacks
     */
    @PrePersist
    protected void onCreate() {
        lastUpdatedAt = createdAt = (Objects.isNull(createdAt) ? DateTime.now() : createdAt);

    }

    @PreUpdate
    protected void onUpdate() {
        lastUpdatedAt = DateTime.now();
        version = version + 1;
    }
}
