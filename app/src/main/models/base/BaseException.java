package models.base;

import codes.ResponseCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created By:  Paridhi
 * Date:        25 July, 2019
 */
public abstract class BaseException extends RuntimeException {

    public BaseException() { super(); }

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseException(Throwable cause) {
        super(cause);
    }

    abstract public ResponseCode getResponseCode();

}
