package exceptions;

import codes.ClientErrorCodes;
import codes.ResponseCode;
import codes.ServerErrorCodes;
import models.base.BaseException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created By:  Paridhi
 * Date:        25 July, 2019
 */
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UnauthorizedException extends BaseException {

    public UnauthorizedException(String message) {
        super(message);
    }

    public UnauthorizedException(Throwable cause) {
        super(cause);
    }

    public UnauthorizedException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public ResponseCode getResponseCode() {
        return ClientErrorCodes.UNAUTHORIZED;
    }

}
