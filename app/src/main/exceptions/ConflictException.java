package exceptions;

import codes.ClientErrorCodes;
import codes.ResponseCode;
import models.base.BaseException;


/**
 * Created By:  Paridhi
 * Date:        25 July, 2019
 */
public class ConflictException extends BaseException {

    public ConflictException(String message) {
        super(message);
    }

    public ConflictException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConflictException(Throwable cause) {
        super(cause);
    }

    @Override
    public ResponseCode getResponseCode() {
        return ClientErrorCodes.CONFLICT;
    }
}
