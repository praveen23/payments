package exceptions;

import codes.ClientErrorCodes;
import codes.ResponseCode;
import models.base.BaseException;


/**
 * Created By:  Paridhi
 * Date:        25 July, 2019
 */
public class BadRequestException extends BaseException {

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadRequestException(Throwable cause) {
        super(cause);
    }

    @Override
    public ResponseCode getResponseCode() {
        return ClientErrorCodes.BAD_REQUEST;
    }
}
