package exceptions;

import codes.ClientErrorCodes;
import codes.ResponseCode;
import models.base.BaseException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created By:  Paridhi
 * Date:        25 July, 2019
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends BaseException {

    public ResourceNotFoundException(String message) {
        super(message);
    }

    public ResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceNotFoundException(Throwable cause) {
        super(cause);
    }

    @Override
    public ResponseCode getResponseCode() {
        return ClientErrorCodes.NOT_FOUND;
    }
}
