package exceptions;

import codes.ResponseCode;
import codes.ServerErrorCodes;
import models.base.BaseException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created By:  Paridhi
 * Date:        25 July, 2019
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerErrorException extends BaseException {

    public InternalServerErrorException(String message) {
        super(message);
    }

    public InternalServerErrorException(Throwable cause) { super(cause);
    }

    public InternalServerErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public ResponseCode getResponseCode() {
        return ServerErrorCodes.INTERNAL_SERVER_ERROR;
    }
}
