package exceptions;

import codes.ResponseCode;
import codes.ServerErrorCodes;
import models.base.BaseException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created By:  Riyaz
 * Date:        29 August, 2019
 */
@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class ServiceUnavailableException extends BaseException {

    public ServiceUnavailableException(String message) {
        super(message);
    }

    public ServiceUnavailableException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceUnavailableException(Throwable cause) {
        super(cause);
    }

    @Override
    public ResponseCode getResponseCode() {
        return ServerErrorCodes.UNAVAILABLE;
    }
}
