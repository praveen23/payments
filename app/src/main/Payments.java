package livspace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created By:  Riyaz
 * Date:        04 July, 2019
 */

@Configuration
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan({"models","controllers", "configurations", "filters", "services","exceptions","codes","models.entities"})
@EntityScan(basePackages = "models")
public class Payments{

	private static final Logger logger = LogManager.getLogger(Payments.class.getName());

	/**
	 * Usage of log4j2 logger.
	 * ThreadContext.put("X-Request-Id", "1323");
	 * logger.info("This is an informational message.");
	 * logger.trace("This is for tracing.");
	 * logger.debug("This is a debugging message.");
	 * logger.error("!This is an error message.");
	 **/
	public static void main(final String[] args) {
	    logger.info("Initiating application bootup");
		SpringApplication app = new SpringApplication(Payments.class);
		app.run(args);
	}

}



