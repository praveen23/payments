package configurations.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
@PropertySource("application.yml")
public class Oauth2ClientService {

    @Value("${oauth2.registrationId}")
    private String registrationId;

    @Value("${oauth2.clientName}")
    private String clientName;

    @Value("${security.oauth2.client.scope}")
    private String[] scopes;

    @Value("${security.oauth2.client.clientId}")
    private String clientId;

    @Value("${security.oauth2.client.clientSecret}")
    private String clientSecret;

    @Value("${security.oauth2.client.userAuthorizationUri}")
    private String userAuthorizationUri;

    @Value("${security.oauth2.client.accessTokenUri}")
    private String accessTokenUri;

    @Value("${security.oauth2.resource.userInfoUri}")
    private String userInfoUri;

    @Value("${security.oauth2.client.redirectUri}")
    private String redirectUriTemplate;

    @Value("${security.oauth2.resource.jwt.keySetUri}")
    private String jwkSetUri;

    @Bean
    public ClientRegistrationRepository clientRegistrationRepository() {
        List<ClientRegistration> registrations = new ArrayList<>();
        registrations.add(getRegistration());
        return new InMemoryClientRegistrationRepository(registrations);
    }

    public ClientRegistration.Builder getBuilder() {
        ClientRegistration.Builder builder = ClientRegistration.withRegistrationId(registrationId);
        builder.scope(Arrays.asList(scopes));
        builder.authorizationUri(userAuthorizationUri);
        builder.tokenUri(accessTokenUri);
        builder.userInfoUri(userInfoUri);
        builder.clientName(clientName);
        builder.clientId(clientId);
        builder.clientSecret(clientSecret);
        builder.redirectUriTemplate(redirectUriTemplate);
        builder.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE);
        builder.clientAuthenticationMethod(ClientAuthenticationMethod.POST);
        builder.jwkSetUri(jwkSetUri);
        return builder;
    }

    private ClientRegistration getRegistration() {
        ClientRegistration clientRegistration = getBuilder().build();
        return clientRegistration;
    }


}
