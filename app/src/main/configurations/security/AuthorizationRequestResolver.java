package configurations.security;

import com.nimbusds.oauth2.sdk.auth.ClientAuthenticationMethod;
import org.springframework.security.crypto.keygen.Base64StringKeyGenerator;
import org.springframework.security.crypto.keygen.StringKeyGenerator;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestResolver;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class AuthorizationRequestResolver implements OAuth2AuthorizationRequestResolver {
    private static int STATE_LENGTH                               = 32;
    private static final String REGISTRATION_ID_URI_VARIABLE_NAME = "registrationId";
    private static final char PATH_DELIMITER                      = '/';
    private final StringKeyGenerator customStateGenerator         = new Base64StringKeyGenerator(Base64.getUrlEncoder(),STATE_LENGTH);
    private final StringKeyGenerator codeVerifierGenerator        = new Base64StringKeyGenerator(Base64.getUrlEncoder().withoutPadding(), 96);
    private ClientRegistrationRepository clientRegistrationRepository;
    private final AntPathRequestMatcher authorizationRequestMatcher;


    public AuthorizationRequestResolver(ClientRegistrationRepository repo, String authorizationRequestBaseUri){
        this.authorizationRequestMatcher = new AntPathRequestMatcher(authorizationRequestBaseUri + "/{" + REGISTRATION_ID_URI_VARIABLE_NAME + "}");
        this.clientRegistrationRepository = repo;
    }

    private String getAction(HttpServletRequest request, String defaultAction) {
        String action = request.getParameter("action");
        if (action == null) {
            return defaultAction;
        }
        return action;
    }

    @Override
    public OAuth2AuthorizationRequest resolve(HttpServletRequest request) {
        String regId = resolveRegistrationId(request);
        String redirectUriAction = getAction(request, "login");
        return resolve(request, regId, redirectUriAction);
    }

    @Override
    public OAuth2AuthorizationRequest resolve(HttpServletRequest request, String registrationId) {
        if (registrationId == null) {
            return null;
        }
        String redirectUriAction = getAction(request, "authorize");
        return resolve(request, registrationId, redirectUriAction);
    }

    private OAuth2AuthorizationRequest resolve(HttpServletRequest request, String registrationId, String redirectUriAction) {
        if (registrationId == null) {
            return null;
        }

        ClientRegistration clientRegistration = clientRegistrationRepository.findByRegistrationId(registrationId);
        if (clientRegistration == null) {
            throw new IllegalArgumentException("Invalid Client Registration with Id: " + registrationId);
        }

        Map<String, Object> attributes = new HashMap<>();
        attributes.put(OAuth2ParameterNames.REGISTRATION_ID, clientRegistration.getRegistrationId());

        OAuth2AuthorizationRequest.Builder builder;
        builder = OAuth2AuthorizationRequest.authorizationCode();
        /**
         * For services in Livspace, it is advised to have a Client Authentication type as POST or BASIC.
         * */
        if (ClientAuthenticationMethod.NONE.equals(clientRegistration.getClientAuthenticationMethod())) {
            Map<String, Object> additionalParameters = new HashMap<>();
            addPkceParameters(attributes, additionalParameters);
            builder.additionalParameters(additionalParameters);
        }

        String redirectUriStr = expandRedirectUri(request, clientRegistration, redirectUriAction);
        String stateVariable = customStateGenerator.generateKey();
        OAuth2AuthorizationRequest authorizationRequest = builder
                .clientId(clientRegistration.getClientId())
                .authorizationUri(clientRegistration.getProviderDetails().getAuthorizationUri())
                .redirectUri(redirectUriStr)
                .scopes(clientRegistration.getScopes())
                .state(stateVariable)
                .additionalParameters(attributes)
                .build();

        return authorizationRequest;
    }

    private String resolveRegistrationId(HttpServletRequest request) {
        if (authorizationRequestMatcher.matches(request)) {
            return this.authorizationRequestMatcher.extractUriTemplateVariables(request).get("registrationId");
        }
        return null;
    }

    private static String expandRedirectUri(HttpServletRequest request, ClientRegistration clientRegistration, String action) {
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("registrationId", clientRegistration.getRegistrationId());

        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(UrlUtils.buildFullRequestUrl(request))
                .replacePath(request.getContextPath())
                .replaceQuery(null)
                .fragment(null)
                .build();
        String scheme = uriComponents.getScheme();
        uriVariables.put("baseScheme", scheme == null ? "" : scheme);
        String host = uriComponents.getHost();
        uriVariables.put("baseHost", host == null ? "" : host);
        // following logic is based on HierarchicalUriComponents#toUriString()
        int port = uriComponents.getPort();
        uriVariables.put("basePort", port == -1 ? "" : ":" + port);
        String path = uriComponents.getPath();
        if (StringUtils.hasLength(path)) {
            if (path.charAt(0) != PATH_DELIMITER) {
                path = PATH_DELIMITER + path;
            }
        }
        uriVariables.put("basePath", path == null ? "" : path);
        uriVariables.put("baseUrl", uriComponents.toUriString());
        uriVariables.put("action", action == null ? "" : action);
        String expandedRedirectUri = UriComponentsBuilder.fromUriString(clientRegistration.getRedirectUriTemplate()).buildAndExpand(uriVariables).toUriString();
        return expandedRedirectUri;
    }


    /**
     *  ----------------------------------------------------------------------------------------------------------
     *      Apps registered on livspace bouncer service have either BASIC or POST as Client authentication type.
     *      None is not recommended.
     *
     *      The below methods are used only when None type is applied. It is recommended to avoid
     * */

    private void addPkceParameters(Map<String, Object> attributes, Map<String, Object> additionalParameters) {
        String codeVerifier = this.codeVerifierGenerator.generateKey();
        attributes.put(PkceParameterNames.CODE_VERIFIER, codeVerifier);
        try {
            String codeChallenge = createCodeChallenge(codeVerifier);
            additionalParameters.put(PkceParameterNames.CODE_CHALLENGE, codeChallenge);
            additionalParameters.put(PkceParameterNames.CODE_CHALLENGE_METHOD, "S256");
        } catch (NoSuchAlgorithmException e) {
            additionalParameters.put(PkceParameterNames.CODE_CHALLENGE, codeVerifier);
        }
    }


    private String createCodeChallenge(String codeVerifier) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] digest = md.digest(codeVerifier.getBytes(StandardCharsets.US_ASCII));
        return Base64.getUrlEncoder().withoutPadding().encodeToString(digest);
    }

    /**
     * This interface was copied from
     *
     * https://github.com/spring-projects/spring-security/blob/master/oauth2/
     *      oauth2-core/src/main/java/org/springframework/security/oauth2/core/
     *      endpoint/PkceParameterNames.java
     *
     * Please use from package as soon as 5.2.0 is released, and we upgrade to it.
     *
     * */

    public interface PkceParameterNames {
        String CODE_CHALLENGE           = "code_challenge";
        String CODE_CHALLENGE_METHOD    = "code_challenge_method";
        String CODE_VERIFIER            = "code_verifier";
    }

    /**
     *  ----------------------------------------------------------------------------------------------------------
     */

}