package configurations.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2SsoProperties;
import org.springframework.context.annotation.*;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;


@Configuration
@EnableWebSecurity
@EnableOAuth2Sso
@EnableOAuth2Client
@PropertySource("application.yml")
public class Oauth2 extends WebSecurityConfigurerAdapter {

    private static String DELIMITER = "/";

    @Value("${oauth2.registrationId}")
    private String registrationId;

    @Value("${oauth2.authorizationUri}")
    private String authorizationUri;

    @Value("${oauth2.redirectionUri}")
    private String redirectionUri;

    @Autowired
    Oauth2ClientService oauth2ClientService;

    @Bean
    @Primary
    public OAuth2SsoProperties oAuth2SsoProperties(){
        OAuth2SsoProperties properties =  new OAuth2SsoProperties();
        properties.setLoginPath(authorizationUri+DELIMITER+registrationId);
        return properties;
    }

    public ClientRegistrationRepository getClientRegistrationRepository() {
        return oauth2ClientService.clientRegistrationRepository();
    }

    @Bean
    public AuthorizationRequestResolver customAuthResolver(){
        AuthorizationRequestResolver customAuthRequest = new AuthorizationRequestResolver(getClientRegistrationRepository(), authorizationUri);
        return customAuthRequest;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http
                .antMatcher("/**")
                .cors()
                    .disable()
                .csrf()
                    .disable()
                .anonymous()
                    .disable()
                .formLogin()
                    .disable()
                .httpBasic()
                    .disable()
                .requestMatchers()
                    .antMatchers("/**")
                    .and()
                .authorizeRequests()
                    .antMatchers(redirectionUri+"**",redirectionUri,
                            authorizationUri+"**",authorizationUri,
                            "/**/*.png",
                            "/**/*.gif",
                            "/**/*.svg",
                            "/**/*.jpg",
                            "/**/*.html",
                            "/**/*.css",
                            "/**/*.js")
                        .permitAll()
                    .anyRequest()
                        .authenticated()
                        .and()
                .oauth2Login()
                    .loginPage(authorizationUri+DELIMITER+registrationId)
                    .clientRegistrationRepository(getClientRegistrationRepository())
                    .authorizationEndpoint()
                        .baseUri(authorizationUri)
                        .authorizationRequestResolver(customAuthResolver())
                        .and()
                    .redirectionEndpoint()
                        .baseUri(redirectionUri+DELIMITER+registrationId)
                        .and()
                    .tokenEndpoint()
                        .and()
                    .userInfoEndpoint()
                        .and();
    }
}
