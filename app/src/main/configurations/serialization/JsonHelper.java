package configurations.serialization;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import exceptions.ConflictException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class JsonHelper {

    @Autowired
    ObjectMapper customObjectMapper;

    public JsonNode toJson(Object object){
        try{
            return customObjectMapper.convertValue(object, JsonNode.class);
        }
        catch (IllegalArgumentException iexception){
            throw new ConflictException("Error converting object to Json: "+iexception);
        }
    }

    public String toJsonString(Object object){
        try{
            return customObjectMapper.writeValueAsString(object);
        }
        catch (JsonProcessingException jpe){
            throw new ConflictException("Error converting object to JsonString: ", jpe);
        }
    }

    public JsonNode parse(String jsonData){
        try{
            return customObjectMapper.readValue(jsonData, JsonNode.class);
        }
        catch (IOException ex){
            throw  new ConflictException("Error Parsing Json String to JsonNode: ", ex);
        }
    }

    public <T> T fromJson(JsonNode jsonNode, Class<T> tClass){
        try{
            return customObjectMapper.treeToValue(jsonNode, tClass);
        }
        catch (JsonProcessingException jpe){
            throw  new ConflictException("Error Parsing JsonNode to Class"+ tClass.getName(), jpe);
        }
    }

    public ObjectNode newObject(){
        return customObjectMapper.createObjectNode();
    }
}
