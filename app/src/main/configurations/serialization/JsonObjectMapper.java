package configurations.serialization;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.jsonorg.JsonOrgModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Configuration
public class JsonObjectMapper {

    private static final Logger logger = LogManager.getLogger(JsonObjectMapper.class.getName());

//    @Bean
//    public void initializeRestObjectMapper() {
//        Unirest.setObjectMapper(new com.mashape.unirest.http.ObjectMapper() {
//            ObjectMapper mapper = getObjectMapper();
//
//            public String writeValue(Object value) {
//                try {
//                    return mapper.writeValueAsString(value);
//                } catch(IOException e){
//                    throw new RuntimeException(e);
//                }
//            }
//
//            public <T> T readValue(String value, Class<T> valueType) {
//                try {
//                    return mapper.readValue(value, valueType);
//                } catch (IOException e) {
//                    throw new RuntimeException(e);
//                }
//            }
//        });
//        System.out.println("=====OBJECT MAPPER HAS BEEN SET FOR REST=====");
//    }

    @Bean
    public ObjectMapper customObjectMapper() {
        ObjectMapper  mapper = new ObjectMapper();

        Hibernate5Module hibernateModule = new Hibernate5Module();
        hibernateModule.disable(Hibernate5Module.Feature.USE_TRANSIENT_ANNOTATION);

        SimpleModule bigDecimalModule = new SimpleModule();
        bigDecimalModule.addSerializer(BigDecimal.class, new BigDecimalSerializer());

        mapper.registerModule(hibernateModule);
        mapper.registerModule(bigDecimalModule);
        mapper.registerModule(new JsonOrgModule());
        mapper.registerModule(new JavaTimeModule());
        mapper.registerModule(new JodaModule());

        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        mapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

        return mapper;
    }

}
