package codes;

import org.springframework.http.HttpStatus;

public interface ResponseCode {
    enum ResponseStatus{
        SUCCESS,
        FAILURE
    }

    public String getMessage();

    public HttpStatus getStatus();

    public ResponseStatus getResponseStatus();

}
