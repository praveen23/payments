package codes;

import org.springframework.http.HttpStatus;

/**
 *  Created By:     Riyaz
 *  Date:           8 Sep, 2019
 */

public enum ClientErrorCodes implements ResponseCode{
    BAD_REQUEST(HttpStatus.BAD_REQUEST, "Bad Request"),
    UNAUTHORIZED(HttpStatus.UNAUTHORIZED, "Unauthorized"),
    PAYMENT_REQUIRED(HttpStatus.UNAUTHORIZED, "Payment Required"),
    FORBIDDEN(HttpStatus.FORBIDDEN, "Forbidden"),
    NOT_FOUND(HttpStatus.NOT_FOUND, "Not Found"),
    CONFLICT(HttpStatus.CONFLICT, "Conflict"),
    NOT_ACCEPTABLE(HttpStatus.BAD_REQUEST, "Not Acceptable"),
    PAYLOAD_TOO_LARGE(HttpStatus.BAD_REQUEST, "Payload Too Large"),
    UNPROCESSABLE_ENTITY(HttpStatus.UNPROCESSABLE_ENTITY, "Unprocessable Entity"),
    TOO_MANY_REQUESTS(HttpStatus.BAD_REQUEST, "Too Many Requests"),
    CLIENT_CLOSED_REQUEST(HttpStatus.UNPROCESSABLE_ENTITY, "Client Closed Request");

    HttpStatus      status;
    String          message;
    ResponseStatus  responseStatus;

    ClientErrorCodes(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
        this.responseStatus = ResponseStatus.FAILURE;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }
}
