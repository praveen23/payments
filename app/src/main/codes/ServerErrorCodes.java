package codes;

import org.springframework.http.HttpStatus;

/**
 *  Created By:     Riyaz
 *  Date:           8 Sep, 2019
 */

public enum ServerErrorCodes implements ResponseCode{
    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "Internal Server Error"),
    NOT_IMPLEMENTED(HttpStatus.INTERNAL_SERVER_ERROR, "Not Implemented"),
    UNAVAILABLE(HttpStatus.SERVICE_UNAVAILABLE, "Unavailable");

    HttpStatus      status;
    String          message;
    ResponseStatus  responseStatus;

    ServerErrorCodes(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
        this.responseStatus = ResponseStatus.FAILURE;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }

    public String getMessage() {
        return message;
    }
}
