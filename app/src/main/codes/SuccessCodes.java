package codes;

import org.springframework.http.HttpStatus;

/**
 *  Created By:     Riyaz
 *  Date:           8 Sep, 2019
 */

public enum SuccessCodes implements ResponseCode{
    OK(HttpStatus.OK, "OK"),
    CREATED(HttpStatus.CREATED, "Created Successfully"),
    ACCEPTED(HttpStatus.CREATED, "Accepted"),
    NO_CONTENT(HttpStatus.NO_CONTENT, "No Content");

    HttpStatus      status;
    String          message;
    ResponseStatus  responseStatus;

    SuccessCodes(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
        this.responseStatus = ResponseStatus.SUCCESS;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }

}
