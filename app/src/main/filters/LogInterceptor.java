package filters;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created By Paridhi on 02/08/2019.
 * This class will be used for adding contextual data to the logs.
 * The preHandle() method will be executed before processing of any user request
 * and the afterCompletion() method will be executed after the response has been rendered to the user.
 */

@Component
public class LogInterceptor implements HandlerInterceptor {

    private static final String REQUEST_ID = "X-Request-Id";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String request_id = request.getHeader(REQUEST_ID);
        if (request_id != null) {
            ThreadContext.put(REQUEST_ID, request_id);
        } else {
            ThreadContext.put(REQUEST_ID, "");
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
                                Exception exception) throws Exception {
        ThreadContext.clearMap();
    }
}

