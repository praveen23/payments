package filters;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created by Paridhi on 02/08/2019.
 * This class is for registering the custom LoggerInterceptor with Spring.
 */
@Configuration
public class MvcConfiguration implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this.LogInterceptor());
    }

    @Bean
    public LogInterceptor LogInterceptor() {
        return new LogInterceptor();
    }
}
