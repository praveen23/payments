package services.util.api.request;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class ApiParams {

    private Filter  filter = new Filter(null);
    private Include include = new Include(null);
    private Sort    sort = new Sort(null);
    private Long    start = 0L;
    private Long    count = 100L;

    private ApiParams(){}

    public ApiParams(String filterString, String includeString, String sortString, Long start, Long count) {
        this.buildFilter(filterString)
                .buildInclude(includeString)
                .buildSort(sortString)
                .buildStart(start)
                .buildCount(count);
    }

    public static ApiParams getInstance(){
        return new ApiParams();
    }

    public ApiParams buildFilter(String filter) {
        this.filter = new Filter(filter);
        return this;
    }

    public ApiParams buildInclude(String include) {
        this.include = new Include(include);
        return this;
    }

    public ApiParams buildSort(String sort) {
        this.sort = new Sort(sort);
        return this;
    }

    public ApiParams buildStart(Long start) {
        if( start !=null )
            this.start = start;
        return this;

    }

    public ApiParams buildCount(Long count) {
        if(count != null)
            this.count = count;
        return this;
    }
}
