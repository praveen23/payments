package services.util.api.response;

import codes.ServerErrorCodes;
import codes.SuccessCodes;
import configurations.serialization.JsonHelper;
import models.base.BaseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * Created By:  Riyaz
 * Date:        08 September, 2019
 */

@Component
public class ResultMapper {

    private static final Logger logger = LogManager.getLogger(ResultMapper.class.getName());

    @Autowired
    JsonHelper jsonHelper;

    public ResponseEntity map(RuntimeException exception){
        ApiResponse apiResponse;
        if(exception instanceof BaseException)
            apiResponse = new ApiResponse(((BaseException)exception).getResponseCode(), null, exception.getMessage());
        else
            apiResponse = new ApiResponse(ServerErrorCodes.INTERNAL_SERVER_ERROR, null, exception.getMessage());
        return map(apiResponse);
    }

    public ResponseEntity map(ApiResponse apiResponse){
        logger.info("Api Response: \n"+ apiResponse.toString());
        return ResponseEntity.status(apiResponse.getResponseCode().getStatus()).body(apiResponse);
    }

    public ResponseEntity map(Object object){
        ApiResponse apiResponse = new ApiResponse(SuccessCodes.OK, object);
        return map(apiResponse);
    }

    public ResponseEntity map(Page page){
        ApiResponse apiResponse = new ApiResponse(SuccessCodes.OK, page);
        return map(apiResponse);
    }

    public ResponseEntity map(Object object, ApiResponse apiResponse){
        apiResponse.setData(object);
        return map(apiResponse);
    }
}
