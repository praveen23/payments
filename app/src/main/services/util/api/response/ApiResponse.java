package services.util.api.response;

import codes.ResponseCode;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Created By:  Riyaz
 * Date:        08 September, 2019
 */

@Setter
@Getter
public class ApiResponse{

    @JsonIgnore
    private ResponseCode    responseCode;

    @JsonIgnore
    private String          exceptionMessage;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object          data;

    @JsonProperty("_total_count")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long            totalCount;

    @JsonProperty("_size")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long            size;

    @JsonProperty("_start")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long            start;

    @JsonProperty("_status")
    private String          responseStatus;

    @JsonProperty("_code")
    private Integer         statusCode;

    @JsonProperty("_message")
    private String          responseMessage;

    public ApiResponse(ResponseCode responseCode, Object object, String exceptionMessage){
        this.responseCode = responseCode;
        this.data = object;
        this.responseStatus = responseCode.getResponseStatus().toString();
        this.statusCode = responseCode.getStatus().value();
        this.exceptionMessage = exceptionMessage;
        this.responseMessage = (exceptionMessage==null ? responseCode.getMessage() : exceptionMessage);
    }

    public ApiResponse(ResponseCode responseCode, Page page){
        this.responseCode = responseCode;
        this.data = page.getRecords();
        this.responseStatus = responseCode.getResponseStatus().toString();
        this.statusCode = responseCode.getStatus().value();
        this.responseMessage = responseCode.getMessage();
        this.start = page.getStart();
        this.totalCount = page.getTotalCount();
        this.size = page.getSize();
    }

    public ApiResponse(ResponseCode responseCode, Object object){
        this.responseCode = responseCode;
        this.data = object;
        this.responseStatus = responseCode.getResponseStatus().toString();
        this.statusCode = responseCode.getStatus().value();
        this.responseMessage = responseCode.getMessage();
    }

    public ApiResponse(ResponseCode responseCode){
        this.responseCode = responseCode;
        this.responseStatus = responseCode.getResponseStatus().toString();
        this.statusCode = responseCode.getStatus().value();
        this.responseMessage = responseCode.getMessage();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("_message: "+responseMessage);
        sb.append("; _code: "+statusCode);
        sb.append("; _status: "+responseStatus);
        sb.append("; _start: "+start);
        sb.append("; _size: "+size);
        sb.append("; _total_count: "+totalCount);
        return sb.toString();
    }
}
