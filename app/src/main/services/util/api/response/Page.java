package services.util.api.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class Page<Record> {
    private Long totalCount;
    private Long size;
    private List<Record> records;
    private Long start;

    public Page(long start, long totalCount, List<Record> records) {
        this.totalCount = totalCount;
        if(records != null) {
            this.records = records;
            this.size = Long.valueOf(records.size());
        }
        this.start = start;
    }

    public Long getSize() {
        return size;
    }

    public List<Record> getRecords() {
        return records;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public Long getStart() {
        return start;
    }
}
