package services;

import configurations.serialization.JsonHelper;
import exceptions.UnauthorizedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.security.Principal;


/**
 * Created By:  Riyaz
 * Date:        21 August, 2019
 */

@Service
public class SecurityService {

    private static final Logger logger = LogManager.getLogger(SecurityService.class.getName());

    @Autowired
    JsonHelper jsonHelper;

    public Authentication getCurrentUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication;
    }

    public Principal getCurrentUserPrincipal(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication==null)
            throw new UnauthorizedException("No Authentication object found");
        return (Principal) authentication.getPrincipal();
    }

    public String getCurrentUserBouncerId(){
        Principal principal = getCurrentUserPrincipal();
        if(principal==null)
            throw new UnauthorizedException("No LoggedIn User found");
        return principal.getName();
    }

    public Boolean loggedInUserExists(){
        return (getCurrentUser()!=null && getCurrentUserPrincipal()!=null && getCurrentUserBouncerId()!=null);
    }
}
