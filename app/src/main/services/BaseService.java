package services;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.CaseFormat;
import configurations.serialization.JsonHelper;
import exceptions.ConflictException;
import lombok.Getter;
import models.base.BaseDao;
import models.base.BaseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import services.util.api.request.ApiParams;
import services.util.api.response.Page;

import java.lang.reflect.Field;
import java.util.Collection;

/**
 * Created By:  Riyaz
 * Date:        15 July, 2019
 */

@Getter
@Transactional(rollbackFor = Exception.class)
@Service
public abstract class BaseService<Model extends BaseModel> {

    public abstract BaseDao<Model>  getDao();
    public abstract Class<Model>    getTypeClass();

    @Autowired
    JsonHelper jsonHelper;

    public Model find(Long id) {
        return getDao().find(getTypeClass(), id);
    }

    public Model find(Long id, ApiParams params) {
        Model model = getDao().find(getTypeClass(), id);
        return model;
    }

    public Model save(Model entity) {
        return getDao().save(entity);
    }

    public Model update(Long id, JsonNode jsonData){
        return getDao().edit(applyJson(id, jsonData));
    }

    public Model update(Model model){
        return getDao().edit(model);
    }

    public Model refresh(Model model) {
        return getDao().refresh(model);
    }

    public Model remove(Model model) {
        return getDao().remove(model);
    }

    public Model remove(Long id) {
        return getDao().remove(getTypeClass(), id);
    }

    protected Model applyJson(Long id, JsonNode jsonData){
        Model model  = getDao().find(getTypeClass(), id);
        Collection<String> allowedFields = model.getAllowedEdits();
        try{
            Field[] fields = model.getClass().getDeclaredFields();
            for(Field field: fields){
                String jsonName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, field.getName());
                if(allowedFields !=null && !allowedFields.contains(jsonName))
                    continue;
                if(jsonData.get(jsonName) != null){
                    Object obj = jsonHelper.fromJson(jsonData.get(jsonName), field.getType());
                    field.setAccessible(true);
                    if(obj instanceof BaseModel){
                        BaseModel subItem = (BaseModel)obj;
                        obj = getDao().find(field.getType(), subItem.getId());
                    }
                    field.set(model, obj);
                }
            }
        }
        catch(IllegalAccessException iae){
                throw new ConflictException("Error setting field value for model: "+ getTypeClass(), iae);
        }
        catch(RuntimeException ex){
            throw new ConflictException("Error editing the model: "+getTypeClass()+" data with id: "+id, ex );
        }
        return model;
    }

    public Page<Model> getAll(ApiParams apiParams){
        Page<Model> page = getDao().getAll(getTypeClass(), apiParams);
        attachEntityBulk(page, apiParams);
        return page;
    }

    protected void attachEntity(Model entity, ApiParams apiParams){
    }

    protected void attachEntityBulk(Page<Model> page, ApiParams apiParams){
        for (Model records: page.getRecords()){
            attachEntity(records, apiParams);
        }
    }

}
