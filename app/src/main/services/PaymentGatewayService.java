package services;

import models.base.BaseDao;
import models.dao.PaymentGatewayDao;
import models.entities.PaymentGatewayModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created By:  Riyaz
 * Date:        05 September, 2019
 */

@Service
public class PaymentGatewayService extends BaseService<PaymentGatewayModel> {

    @Autowired
    private PaymentGatewayDao paymentGatewayDao;

    @Override
    public BaseDao<PaymentGatewayModel> getDao() {
        return paymentGatewayDao;
    }

    @Override
    public Class<PaymentGatewayModel> getTypeClass() {
        return PaymentGatewayModel.class;
    }

}
