###
#     Payment Gateway table
#

CREATE TABLE `payment_gateway`(
    `id`                          bigint(20) NOT NULL AUTO_INCREMENT,
    `name`                        varchar(100) NOT NULL,
    `short_name`                  varchar(15),
    `code`                        varchar(5) NOT NULL,
    `description`                 text,
    `status`                      enum(''INACTIVE'',''ACTIVE'') DEFAULT ''INACTIVE'',
    `created_at`                  datetime NOT NULL,
    `last_updated_at`             datetime NOT NULL,
    `created_by`                  bigint(20) DEFAULT NULL,
    `last_updated_by`             bigint(20) DEFAULT NULL,
    `version`                     int(11) UNSIGNED NOT NULL,
     PRIMARY KEY (`id`)
);


INSERT INTO `payment_gateway`(`id`,`name`,`short_name`,`code`,`description`,`status`,`created_at`,`last_updated_at`,`version`)
VALUES
(1,''PayU biz India'',''payu'',''PAYU'',''PayU is a fintech company that provides payment technology to online merchants. The company was founded in 2002 and is headquartered in Hoofddorp, Netherlands. It allows online businesses to accept and process payments through payment methods that can be integrated with web and mobile applications.'',''INACTIVE'',''2019-08-30 17:53:32'',''2019-08-30 17:53:32'',1),
(2,''Ingenico Group'',''ingeico'',''INGC'',''Ingenico is a French-based company, whose business is to provide the technology involved in secure electronic transactions. Its traditional business is based on the manufacture of point of sale (POS) payment terminals, but it also includes complete payment software and related services, also software for merchants.'',''INACTIVE'',''2019-08-30 17:57:12'',''2019-08-30 17:57:12'',1),
(3,''Braintree Group'',''braintree'',''BRT'',''Braintree, a division of PayPal, is a company based in Chicago that specializes in mobile and web payment systems for e-commerce companies. The company was acquired by PayPal on September 26, 2013.'',''INACTIVE'',''2019-08-30 18:02:23'',''2019-08-30 18:02:23'',1),
(4,''Stripe Payments India'',''stripe'',''STRP'',''Stripe is an American technology company based in San Francisco, California. Its software allows individuals and businesses to make and receive payments over the Internet. Stripe provides the technical, fraud prevention, and banking infrastructure required to operate online payment systems.'',''INACTIVE'',''2019-08-30 18:03:46'',''2019-08-30 18:03:46'',1);


CREATE TABLE `payment_request` (
    `id`                          bigint(20) NOT NULL AUTO_INCREMENT,
    `uuid`                        varchar(100) NOT NULL,
    `amount`                      decimal (10,2) NOT NULL,
    `requester`                   enum(''CANVAS'') NOT NULL,
    `requester_entity_id`         bigint(20),
    `requester_entity_name`       varchar(100),
    `callback_url`                varchar(255),
    `preferred_gateway_id`        varchar(100),
    `status`                      enum(''INITIATED''),
    `created_at`                  datetime NOT NULL,
    `last_updated_at`             datetime NOT NULL,
    `created_by`                  bigint(20) DEFAULT NULL,
    `last_updated_by`             bigint(20) DEFAULT NULL,
    `deleted`                     tinyint(1) DEFAULT 0,
    `version`                     int(11) UNSIGNED NOT NULL,
     PRIMARY KEY (`id`)
);

CREATE TABLE `actions` (
    `id`                          int(11) NOT NULL AUTO_INCREMENT,
    `name`                        varchar(255) NOT NULL,
    `created_at`                  datetime NOT NULL,
    `last_updated_at`             datetime NOT NULL,
    `created_by`                  bigint(20) DEFAULT NULL,
    `last_updated_by`             bigint(20) DEFAULT NULL,
    `deleted`                     tinyint(1) DEFAULT 0,
    `version`                     int(11) UNSIGNED NOT NULL,
     PRIMARY KEY (`id`)
);

CREATE TABLE `payments` (
    `id`                          bigint(20) NOT NULL AUTO_INCREMENT,
    `request_id`       			  bigint(20) NOT NULL,
    `amount_captured`             decimal (10,2) NOT NULL,
    `status`                      enum(''declined'') NOT NULL,
    `gateway_id`                  bigint(20) NOT NULL,
    `gateway_charge`              decimal(10,2) NOT NULL,
    `gateway_message`		      text,
    `gateway_txn_id`			  varchar(255) NOT NULL,
    `payment_channel`		      enum(''CREDIT CARD'') NOT NULL,
    `gateway_error`			      boolean NOT NULL,
    `gateway_time_taken`		  bigint(20) NOT NULL,
    `gateway_receipt_url`		  text,
    `is_disputed`			      boolean NOT NULL,
    `disputed_payment_id`		  bigint(20),
    `created_at`                  datetime NOT NULL,
    `last_updated_at`             datetime NOT NULL,
    `created_by`                  bigint(20) DEFAULT NULL,
    `last_updated_by`             bigint(20) DEFAULT NULL,
    `deleted`                     tinyint(1) DEFAULT 0,
    `version`                     int(11) UNSIGNED NOT NULL,
     PRIMARY KEY (`id`),
     KEY `request_id` (`request_id`),
     KEY `gateway_id` (`gateway_id`),
     CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`request_id`) REFERENCES `payment_request` (`id`),
     CONSTRAINT `payment_ibfk_2` FOREIGN KEY (`gateway_id`) REFERENCES `payment_gateway` (`id`)
);

CREATE TABLE `gateway_failures`(
    `id`				    bigint(20) NOT NULL AUTO_INCREMENT,
	`gateway_id`		    bigint(20) NOT NULL,
	`payment_request_id`	bigint(20) NOT NULL,
	`payment_id`		    bigint(20) NOT NULL,
	`gateway_status`		varchar(100) NOT NULL,
	`gateway_message`	    varchar(255) NOT NULL,
	`gateway_code`		    varchar(55),
	`gateway_response`	    text,
	`created_at`            datetime NOT NULL,
    `last_updated_at`       datetime NOT NULL,
    `created_by`            bigint(20) DEFAULT NULL,
    `last_updated_by`       bigint(20) DEFAULT NULL,
    `deleted`               tinyint(1) DEFAULT 0,
    `version`               int(11) UNSIGNED NOT NULL,
     PRIMARY KEY (`id`),
     KEY `payment_request_id` (`payment_request_id`),
     KEY `gateway_id` (`gateway_id`),
     KEY `payment_id` (`payment_id`),
     CONSTRAINT `gateway_failures_1` FOREIGN KEY (`payment_request_id`) REFERENCES `payment_request` (`id`),
     CONSTRAINT `gateway_failures_2` FOREIGN KEY (`gateway_id`) REFERENCES `payment_gateway` (`id`),
     CONSTRAINT `gateway_failures_3` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`)
);


CREATE TABLE `events`(
    `id`				    bigint(20) NOT NULL,
	`payment_request_id`	bigint(20),
	`payment_id`		    bigint(20),
	`gateway_failure_id`	bigint(20),
	`gateway_id`		    bigint(20),
	`action_taken`		    int(11) NOT NULL,
	`message`			    varchar(255),
	`notes`			        text,
	`created_at`            datetime NOT NULL,
    `last_updated_at`       datetime NOT NULL,
    `created_by`            bigint(20) DEFAULT NULL,
    `last_updated_by`       bigint(20) DEFAULT NULL,
    `deleted`               tinyint(1) DEFAULT 0,
    `version`               int(11) UNSIGNED NOT NULL,
     PRIMARY KEY (`id`),
     KEY `payment_request_id` (`payment_request_id`),
     KEY `gateway_id` (`gateway_id`),
     KEY `payment_id` (`payment_id`),
     KEY `gateway_failure_id` (`gateway_failure_id`),
     CONSTRAINT `events_1` FOREIGN KEY (`payment_request_id`) REFERENCES `payment_request` (`id`),
     CONSTRAINT `events_2` FOREIGN KEY (`gateway_id`) REFERENCES `payment_gateway` (`id`),
     CONSTRAINT `events_3` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`),
     CONSTRAINT `events_4` FOREIGN KEY (`gateway_failure_id`) REFERENCES `gateway_failures` (`id`)
);



